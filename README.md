# btg-httpsession

#### 项目说明
> 实现了httpsession标准适用于普通web项目和jfinal项目的分布式session

#### 使用方式
```
一、配置方式：
1、通过filter配置(非jfinal项目推荐配置方式)；
    /**
     *
     * BTGHttpSessionRequest替换filter(local session管理)
     * 注意：此filter优先级必须大于业务系统处理相关filter
     *
     * 若需要redis、db模式的session管理：
     * 1、请在初始化配置参数init-param
     *     sessionDao：可选，默认为local。
     *        local-使用local的BTGSessionDao；
     *        redis-使用redis的BTGSessionDao
     *        db-使用redis的BTGSessionDao；
     *     dname：可选，默认为jfinal默认；
     *        若sessionDao为redis，该参数则表示RedisPlugin的name；
     *        若sessionDao为db，该参数表示ActiveRecordPlugin的name；
     * 2、或者扩展此类，重写init方法完成sessionDao的初始化即可
     */
    <filter>
        <filter-name>btgsession</filter-name>
        <filter-class>cn.usbtg.httpsession.servlet.filter.BTGHttpSessionFilter</filter-class>
        <init-param>
            <param-name>sessionDao</param-name>
            <param-value>redis</param-value>
        </init-param>
        <init-param>
            <param-name>dname</param-name>
            <param-value>myredis</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>btgsession</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

2、(jfinal项目推荐)使用jfinal的plugin+interceptor(interceptor或handler，二选一)配置(若使用默认session管理容器无需配置plugin
    ，仅当需要使用非默认BTGSessionDao实现时，需要在plugin中进行初始化)；
    JfinalConfig：可选配置，用户初始化sessionDao实现方案
        public void configPlugin(Plugins me) {
            //local(默认，无需配置)
            BTGSessionDao localSessionDao = new BTGLocalSessionDao();
            BTGSessionPlugin sessionPlugin = new BTGSessionPlugin(localSessionDao);
            me.add(sessionPlugin);

            //redis
            BTGSessionDao redisSessionDao = new BTGRedisSessionDao();
            BTGSessionDao redisSessionDao = new BTGRedisSessionDao("myredis");
            BTGSessionPlugin sessionPlugin = new BTGSessionPlugin(redisSessionDao);
            me.add(sessionPlugin);

            //db
            BTGSessionDao dbSessionDao = new BTGDBSessionDao();
            BTGSessionDao dbSessionDao = new BTGDBSessionDao("mydb");
            BTGSessionPlugin sessionPlugin = new BTGSessionPlugin(dbSessionDao);
            me.add(sessionPlugin);
        }

    Interceptor：必须配置，用于替换session管理机制
        me.add(new BTGHttpSessionInterceptor());

    Handler：必须配置，用于替换session管理机制
        me.add(new BTGHttpSessionHandler());

二、调用API：
1、使用BTGHttpSessionContext接口的实现类BTGStandardSessionContext直接完成对session的操作；
    getSession(String sessionId):BTGStandardSessionContext.getSessionContext().getSession(sessionId)
    getNewSession():BTGStandardSessionContext.getSessionContext().getNewSession()
    更多api请查看BTGSessionContext接口

2、(推荐)使用静态访问类SessionKit完成对session的操作；
    getSession(String sessionId):SessionKit.getSession(sessionId)
    getNewSession():SessionKit.getNewSession()
    更多api请查看SessionKit类
```

#### 升级记录
```
v4.0.5
1、修复session过期时间秒被当做毫秒处理的bug；

v4.0.4
1、session超时时间单位由分钟更换为秒；

v4.0.3
1、session超时时间单位由毫秒更换为分钟；
2、过期session清理机制触发周期单位由毫秒更换为秒；
3、session最后访问时间同步更新机制触发周期默认值更改为实时；

v4.0.2
1、jfinal升级到3.6；

v4.0.1
1、转为独立maven依赖，去掉parent；

v3.0.3
1、btg-parent升级到v2.0.1；

v3.0.2
1、统一依赖管理；

v3.0.1
1、统一迁移至公司名下；

v2.0.1
1、转为maven项目；
2、整理包名；

v1.2.1
1、修复session刷新时cookie未延长有效时间的bug（interceptor和handler）；

v1.1.1.201706161359
1、修复session刷新时cookie未延长有效时间的bug；

v1.1.1.201706151713
1、考虑到jar包以后维护可能会比较频繁，特统一版本号命名方式；

v1.0.1704071433：
1、filter增加参数支持；

v1.0.1704071018：
1、修复cookie刷新逻辑；
2、修改使用说明(README.md)；

v1.0.1704061218：
1、增加jfinal的handler；
2、BTGSessionPlugin；
3、修正filter逻辑；
4、增加禁用简单单点登录的配置；
5、修正intercepter逻辑；

v1.0.1704011125：
1、session管理器刷新session修复bug；

v1.0.1703290911：
1、防止更新太频繁影响性能，增加session最后访问时间更细同步机制多重策略配置，内置三种方式，
   实时(设置为0)，间隔时间(默认，30000毫秒)，不更新(设置为小于0)；
2、调整session最后访问时间更新算法，最后访问时间大于上次访问时间时更新；
3、BTGSession增加克隆方法；
4、调整session刷新更新算法，保留原有session参数(重复时使用新值覆盖旧值的方式处理)；

v1.0.1703240949：
1、设置默认cookie path为项目名称(原始默认为/)；

v1.0.20170321：
1、修复BTGSessionPlugin设置cookie的path无效的bug；

v1.0.4：
1、修复项目停止后，session被清空的bug(插件停止调用了clear()方法)；

v1.0.3：
1、修改session cookie的path为可配置；

v1.0.2：
1、修正对getSession()和getSession(boolean create)的误解(查询HttpServlet官方文档，getSession()与getSession(true)等同；
2、增加自动写入session cookie；
3、取消filter方式destroy时对session容器的清空操作；
4、基于DB的过期session清理机制进行优化，性能大幅度提升；
5、将基于DB存放session的建表sql脚本加入进来(暂时只贴了mysql的，其他数据库后续补充)；

v1.0.1：
1、基于原有btg-session-jfinal版本，完成对HttpSession的实现；
2、提供两种使用配置方式，filter和jfinal的plugin+interceptor；
3、更好的session过期清理策略；
4、二级缓存策略优化；
5、性能提升；
```
---
豆圆