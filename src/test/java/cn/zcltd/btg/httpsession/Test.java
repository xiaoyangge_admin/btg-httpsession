package cn.zcltd.btg.httpsession;

import cn.zcltd.btg.httpsession.impl.BTGDBSessionDao;
import cn.zcltd.btg.httpsession.impl.BTGLocalSessionDao;
import cn.zcltd.btg.httpsession.impl.BTGRedisSessionDao;
import cn.zcltd.btg.httpsession.impl.BTGStandardSession;
import cn.zcltd.btg.httpsession.kit.SessionKit;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.redis.RedisPlugin;

/**
 * Tester.
 */
public class Test {

    /**
     * Method: BTGStandardSession.getValueNames()
     */
    @org.junit.Test
    public void testGetValueNames() throws Exception {
        BTGStandardSession btgStandardSession = new BTGStandardSession("sessionid222", 3);
        btgStandardSession.setAttribute("a", "aaa");
        btgStandardSession.setAttribute("b", "bbb");
        String[] aa = btgStandardSession.getValueNames();
        for (String a : aa) {
            System.out.println(a);
        }
    }

    /**
     * Method: BTGSessionDao
     */
    public void testBTGSessionDao(BTGSessionDao sessionDao, boolean isUseCache) throws Exception {
        System.out.println("size:" + sessionDao.getSessions().size());

        JSONObject dataBean = new JSONObject();
        dataBean.put("name", "zhangsan");
        dataBean.put("age", 30);

        BTGSessionContext sessionContext = SessionKit.getSessionContext(null, sessionDao);
        sessionContext.setUseCache(isUseCache);
        sessionContext.setSessionTimeoutSeconds(10); //10秒过期
        sessionContext.setMaxClearTimeoutSeconds(5);//5秒清理一次
        for (int i = 0; i < 20; i++) {
            dataBean.put("index", i);

            //获取一个全新的session
            BTGSession session = sessionContext.getNewSession();
            //为session设置值
            session.setAttribute("user", dataBean);
            //根据sessionid重新获取该session
            BTGSession getSession = sessionContext.getSession(session.getId());
            //获取session中存储的值
            System.out.println(getSession.getAttribute("user"));
            System.out.println("size:" + sessionDao.getSessions().size());

            Thread.sleep(1000);
        }

        System.out.println("size:" + sessionDao.getSessions().size());
    }

    /**
     * Method: BTGLocalSessionDao
     */
    @org.junit.Test
    public void testBTGLocalSessionDao() throws Exception {
        BTGSessionDao sessionDao = new BTGLocalSessionDao();

        testBTGSessionDao(sessionDao, false);
    }

    /**
     * Method: BTGRedisSessionDao
     */
    @org.junit.Test
    public void testBTGRedisSessionDao() throws Exception {
        RedisPlugin redisPluginSession = new RedisPlugin("test", "127.0.0.1", 6379, "admin123");
        redisPluginSession.start();

        //BTGSessionDao sessionDao = new BTGRedisSessionDao();
        BTGSessionDao sessionDao = new BTGRedisSessionDao("test");

        testBTGSessionDao(sessionDao, false);
    }

    /**
     * Method: BTGDBSessionDao
     */
    @org.junit.Test
    public void testBTGDBSessionDao() throws Exception {
        //初始化连接池
        String JDBC_URL = "jdbc:mysql://127.0.0.1:3306/loapy?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull";
        String JDBC_USERNAME = "root";
        String JDBC_PASSWORD = "admin123";
        DruidPlugin druidPlugin = new DruidPlugin(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD);
        druidPlugin.start();

        //初始化ActiveRecord
        ActiveRecordPlugin arp = new ActiveRecordPlugin("test", druidPlugin);
        arp.setDialect(new MysqlDialect());
        arp.setShowSql(true);
        arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));
        arp.start();

        //BTGSessionDao sessionDao = new BTGDBSessionDao();
        BTGSessionDao sessionDao = new BTGDBSessionDao("test");

        testBTGSessionDao(sessionDao, true);
    }
}