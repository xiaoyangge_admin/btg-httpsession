package cn.zcltd.btg.httpsession;

import javax.servlet.http.HttpSession;
import java.io.Serializable;

/**
 * BTGSession，扩展自HttpSession
 */
public interface BTGSession extends HttpSession, Serializable {

    /**
     * 设置最后激活时间
     *
     * @param lastAccessedTime long
     */
    void setLastAccessedTime(long lastAccessedTime);

    /**
     * 是否失效
     *
     * @return boolean
     */
    public boolean isInvalidate();

    /**
     * 使session处于活跃状态
     */
    public void active();

    /**
     * 获取session上下文
     *
     * @return 上下文
     */
    @Override
    BTGSessionContext getSessionContext();

    /**
     * 克隆
     *
     * @return 克隆的对象
     */
    public BTGSession cloneMe();

    /**
     * 添加属性不进行session同步更新
     *
     * @param key 参数名
     * @param obj 参数值
     */
    public void addAttributeNotRefresh(String key, Object obj);

    /**
     * 获取session最后访问时间最后同步更新时间
     *
     * @return session最后访问时间最后同步更新时间
     */
    public long getLastUpdateMillis();

    /**
     * 设置session最后访问时间最后同步更新时间
     *
     * @param lastUpdateMillis session最后访问时间最后同步更新时间
     */
    public void setLastUpdateMillis(long lastUpdateMillis);
}