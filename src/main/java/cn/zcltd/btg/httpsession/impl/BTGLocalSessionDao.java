package cn.zcltd.btg.httpsession.impl;

import cn.zcltd.btg.core.exception.BtgRuntimeException;
import cn.zcltd.btg.httpsession.BTGSession;
import cn.zcltd.btg.httpsession.BTGSessionDao;
import cn.zcltd.btg.sutil.EmptyUtil;

import java.util.Hashtable;
import java.util.Iterator;

/**
 * 基于本地容器的session存储实现
 */
public class BTGLocalSessionDao implements BTGSessionDao {
    private static Hashtable<String, BTGSession> sessionContext = new Hashtable<>();//session容器

    @Override
    public void saveSession(BTGSession session) {
        if (EmptyUtil.isEmpty(session)) {
            throw new BtgRuntimeException("session is null");
        }
        sessionContext.put(session.getId(), session);
    }

    @Override
    public void deleteSession(String sessionId) {
        if (EmptyUtil.isEmpty(sessionId)) {
            throw new BtgRuntimeException("session id is null");
        }
        sessionContext.remove(sessionId);
    }

    @Override
    public BTGSession getSession(String sessionId) {
        return sessionContext.get(sessionId);
    }

    @Override
    public void refreshSession(BTGSession session) {
        if (EmptyUtil.isEmpty(session)) {
            throw new BtgRuntimeException("session is null");
        }
        sessionContext.put(session.getId(), session);
    }

    @Override
    public void active(BTGSession session) {
        if (EmptyUtil.isEmpty(session)) {
            throw new BtgRuntimeException("session is null");
        }
        sessionContext.get(session.getId()).active();
    }

    @Override
    public Hashtable<String, BTGSession> getSessions() {
        return sessionContext;
    }

    @Override
    public void clearTimeout() {
        Iterator<BTGSession> iterator = sessionContext.values().iterator();
        while (iterator.hasNext()) {
            BTGSession session = iterator.next();
            if (session.isInvalidate()) {
                iterator.remove();
            }
        }
    }

    @Override
    public void clear() {
        sessionContext.clear();
    }
}